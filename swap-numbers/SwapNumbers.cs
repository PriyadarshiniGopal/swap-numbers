﻿using System;

namespace swap_numbers
{
    class SwapNumbers
    {
        /// <summary>
        /// Swapping two numbers
        /// </summary>
        /// <param name="num1">First Number</param>
        /// <param name="num2">Second Number</param>
        static void Swap(ref int num1, ref int num2)
        {
            int temp = num1;
            num1 = num2;
            num2 = temp;
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Enter Two Numbers to swap");
            int num1 = Convert.ToInt32(Console.ReadLine());
            int num2 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Two Numbers before swap");
            Console.WriteLine($"{num1}  {num2}");
            Swap(ref num1, ref num2);
            Console.WriteLine("Two Numbers After swap");
            Console.WriteLine($"{num1}  {num2}");
        }
    }
}
